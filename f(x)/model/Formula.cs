﻿using org.mariuszgromada.math.mxparser;
using System;

namespace f_x_.model
{
    public class Formula
    {
        private string exp;
        private bool isSetA;
        private bool isSetB;
        private bool isSetC;
        private bool isSetQ;
        private int aVar;
        private int bVar;
        private int cVar;
        private int qVar;

        public Formula()
        {
        }
        public Formula(string text)
        {
            exp = text;
        }
        public void SetFormula(string text)
        {
            exp = text;
        }
        public string expression()
        {
            return exp;
        }

        public int AVar
        {
            set
            {
                aVar = value;
                isSetA = true;
            }
        }
        public int BVar
        {
            set
            {
                bVar = value;
                isSetB = true;
            }
        }
        public int CVar
        {
            set
            {
                cVar = value;
                isSetC = true;
            }
        }
        public int QVar
        {
            set
            {
                qVar = value;
                isSetQ = true;
            }
        }
        public double GetValue(double valueX)
        {
            if (String.IsNullOrEmpty(exp))
                return 0;
            Expression tmpe = new Expression(exp);
            tmpe.defineArgument("x", valueX);
            if (isSetA)
                tmpe.defineArgument("a", aVar);
            if (isSetB)
                tmpe.defineArgument("b", bVar);
            if (isSetC)
                tmpe.defineArgument("c", cVar);
            if (isSetQ)
                tmpe.defineArgument("q", qVar);
            return tmpe.calculate();
        }
    }
}
